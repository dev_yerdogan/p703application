package com.example.p703;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

@SpringBootApplication
public class P703Application {

    public static void main(String[] args) {

        SpringApplication.run(P703Application.class, args);
        mainSummationFunc();
    }

    public static void mainSummationFunc(){
        int     num;
        Scanner input = new Scanner(System.in);//create scanner object
        System.out.print("How many # you want to add : ");
        num = input.nextInt();//return num from keyboard
        int[] arr = new int[num];
        int sum=0;
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Enter Num" + (i + 1) + ": ");
            arr[i] = input.nextInt();
            // sum+=arr[i];
        }

        System.out.println(calculate(arr));
    }

    public static int calculate(int[] array) {
        int sum=0;
        for (int i = 0; i < array.length; i++) {
            sum+=array[i];
        }
        return sum;
    }


}
